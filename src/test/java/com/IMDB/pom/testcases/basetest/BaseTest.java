package com.IMDB.pom.testcases.basetest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.IMDB.pom.pages.util.ExtentManager;
import com.IMDB.pom.pages.util.IMDBConstants;
import com.IMDB.pom.pages.util.Xls_Reader;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseTest {

	public WebDriver driver;
	public ExtentReports extent = ExtentManager.getInstance();
	public ExtentTest test;
	public Xls_Reader xls = new Xls_Reader(IMDBConstants.DATA_XLS_PATH);
	public String sheet = IMDBConstants.TESTDATA_SHEET;


	/**
	 * Initializing test setup  
	 * @param bType
	 */
	public void init(String bType){
		test.log(LogStatus.INFO, "Opening browser - >"+bType);
		if(!IMDBConstants.GRID_RUN){
			//Local Machine
		if(bType.equals("Mozilla")){
			System.setProperty("webdriver.gecko.driver",IMDBConstants.GECKO_DRIVER_EXE);
			driver = new FirefoxDriver();
		}
		else if (bType.equals("Chrome")){
			System.setProperty("webdriver.chrome.driver", IMDBConstants.CHROME_DRIVER_EXE);
			driver = new ChromeDriver();
		}
		else if (bType.equals("IE")){
			System.setProperty("webdriver.ie.driver", IMDBConstants.IE_DRIVER_EXE);
			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
			ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			ieCapabilities.setCapability("requireWindowFocus", true);
			ieCapabilities.setCapability("ie.ensureCleanSession", true);
			driver = new InternetExplorerDriver(ieCapabilities);
			//driver = new InternetExplorerDriver();
		}
		}
		else{
			//Grid
			DesiredCapabilities cap = null;
			if(bType.equals("Mozilla")){
				cap = DesiredCapabilities.firefox();
				cap.setBrowserName("firefox");
				cap.setJavascriptEnabled(true);
				cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
			}
			else if(bType.equals("Chrome")){
				cap = DesiredCapabilities.chrome();
				cap.setBrowserName("chrome");
				cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
			}
			else if(bType.equals("IE")){
				cap = DesiredCapabilities.internetExplorer();
				cap.setBrowserName("IE");
				//cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				//cap.setCapability("requireWindowFocus", true);
				//cap.setCapability("ie.ensureCleanSession", true);
				cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
			}
			try{
				driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
			
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			
		//Working with FF V53.0, so using below setSize function. It is not required with latest FF version (55,56)
		if(bType.equals("Mozilla")){
			driver.manage().window().setSize(new Dimension(1920, 1080));
		}else if(!(bType.equals("Mozilla"))){		
		driver.manage().window().maximize();
		}
		test.log(LogStatus.INFO, "Opened "+bType+ " browser successfully");
		if(!(bType.equals("Mozilla"))){		
		driver.manage().window().maximize();
		}
		
		
	}


	/**
	 * Capturing screenshot
	 */
	public void takeScreenshot(){
		Date d = new Date();
		String screenshotFile=d.toString().replace(":","_").replace(" ", "_")+".png";	
		String filePath = IMDBConstants.REPORTS_PATH+"Screenshots\\"+screenshotFile;
		//Store screenshots in that file
		File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try{
			FileUtils.copyFile(srcFile, new File(filePath));
		}catch(IOException e){
			e.printStackTrace();

		}
		test.log(LogStatus.INFO, test.addScreenCapture(filePath));

	}

	/**
	 * Reporting failed test case
	 * @param failureMessage
	 */
	public void reportFailure(String failureMessage){
		test.log(LogStatus.FAIL, failureMessage);
		takeScreenshot();
		Assert.fail("Test failed");
	}	

}

