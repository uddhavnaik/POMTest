package com.IMDB.pom.testcases.frontstore;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.IMDB.pom.pages.IMDBHomePage;
import com.IMDB.pom.pages.LaunchPage;
import com.IMDB.pom.pages.TopMoviesPage;
import com.IMDB.pom.pages.util.DataUtil;
import com.IMDB.pom.pages.util.IMDBConstants;
import com.IMDB.pom.testcases.basetest.BaseTest;
import com.relevantcodes.extentreports.LogStatus;

public class IMDBMoviesTest extends BaseTest{

	String testCaseName="IMDBMoviesTest";

	@Test(dataProvider="getData")
	public void placeOrder(Hashtable<String,String> data){
		test = extent.startTest("IMDB Movies Test");
		if(!DataUtil.isSkip(xls, testCaseName) || data.get(IMDBConstants.RUNMODE_COL).equals("N")){
			test.log(LogStatus.SKIP, "Skipping the test as runmode is NO");
			throw new SkipException("Skipping the test as runmode is NO");
		}
		test.log(LogStatus.INFO, "Starting IMDB Test ");
		test.log(LogStatus.INFO, "Opening Browser");
		init(data.get("Browser"));
		test.log(LogStatus.INFO, "Launched Browser");

		LaunchPage lp = new LaunchPage(driver,test);
		PageFactory.initElements(driver, lp);

		//Navigating to IMDB Home page
		Object page = lp.gotoHomePage();
		if(!(page instanceof IMDBHomePage)){
			takeScreenshot();
			reportFailure("Application is under Maintenance");
		}else{
			test.log(LogStatus.INFO, "Navigated to IMDB Home page successfully");
			takeScreenshot();
		}

		IMDBHomePage hmPage = new IMDBHomePage(driver,test);
		PageFactory.initElements(driver, hmPage);

		//Navigating to top movies page
		if(!(hmPage.goToTopMovies())){
			reportFailure("Could not navigate to Top movies page");
		}else{
			test.log(LogStatus.INFO, "Navigated to Top movies page successfully");
		}

		TopMoviesPage tmPage = new TopMoviesPage(driver,test);
		PageFactory.initElements(driver, tmPage);

		//Fetching movies info
		if(!(tmPage.fetchMoviesInfo())){
			reportFailure("Could not fetch Top movies info");
		}else{
			test.log(LogStatus.INFO, "Fetched Top movies info successfully");
		}

		//Printing movies info
		if(!(tmPage.printMoviesInfo())){
			reportFailure("Could not print Top movies info");
		}else{
			test.log(LogStatus.INFO, "Printed Top movies info successfully");
		}



	}


	@AfterMethod
	public void quit(){
		if(extent != null){
			extent.endTest(test);
			extent.flush();
		}
		if(driver != null)
			driver.quit();

	}

	@DataProvider
	public Object[][] getData(){
		return DataUtil.getData(xls, testCaseName);


	}
}