package com.IMDB.pom.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.IMDB.pom.pages.base.BasePage;
import com.IMDB.pom.pages.util.IMDBConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class LaunchPage extends BasePage{

	@FindBy(xpath=IMDBConstants.MAINTENANCE_PAGE)
	public WebElement maintenancePage;

	/**
	 * Default constructor
	 */
	public LaunchPage(){}

	/**
	 * Parameterized constructor
	 * @param driver
	 * @param test
	 */
	public LaunchPage(WebDriver driver, ExtentTest test){
		super(driver,test);		
	}

	/**
	 * Navigating to IMDB Home page
	 * @return
	 */
	public Object gotoHomePage(){
		test.log(LogStatus.INFO, "Opening the URL - > "+ IMDBConstants.getEnvDetails().get("url"));
		driver.get(IMDBConstants.getEnvDetails().get("url"));
		test.log(LogStatus.INFO, "Opened the URL - > "+ IMDBConstants.getEnvDetails().get("url"));

		boolean underMaintennance = isElementPresent(IMDBConstants.MAINTENANCE_PAGE);
		if(underMaintennance){
			test.log(LogStatus.FATAL, "Application is under Maintenance");
			LaunchPage launchPage = new LaunchPage(driver,test);
			PageFactory.initElements(driver, launchPage);
			return launchPage;
		}
		else if((driver.getTitle().equalsIgnoreCase("404 Not Found"))){
			test.log(LogStatus.FATAL, "Application is under Maintenance");
			LaunchPage launchPage = new LaunchPage(driver,test);
			PageFactory.initElements(driver, launchPage);
			return launchPage;
		}
		else if(!underMaintennance){
			IMDBHomePage hmPage = new IMDBHomePage(driver,test);
			PageFactory.initElements(driver, hmPage);
			return hmPage;
		}
		LaunchPage launchPage = new LaunchPage(driver,test);
		return launchPage;
	}

}	
