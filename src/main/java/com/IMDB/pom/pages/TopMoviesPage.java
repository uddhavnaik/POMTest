package com.IMDB.pom.pages;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.IMDB.pom.pages.base.BasePage;
import com.IMDB.pom.pages.util.IMDBConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TopMoviesPage extends BasePage {

	@FindBy(xpath=IMDBConstants.MOVIE_NAMES)
	public List<WebElement> movieNames;

	@FindBy(xpath=IMDBConstants.MOVIE_YEARS)
	public List<WebElement> movieYears;

	@FindBy(xpath=IMDBConstants.MOVIE_RATINGS)
	public List<WebElement> movieRatings;

	/**
	 * Default constructor
	 */
	public TopMoviesPage() {}

	/**
	 * Parameterized constructor
	 * @param driver
	 * @param test
	 */
	public TopMoviesPage(WebDriver driver, ExtentTest test) {
		super(driver,test);
	}

	/**
	 * Capturing movies info
	 * @return
	 */
	public boolean fetchMoviesInfo() {

		for(int i=0; i<250; i++){			

			String name = movieNames.get(i).getText();		
			String year = movieYears.get(i).getText();
			String rating = movieRatings.get(i).getText();		

			String sql = "INSERT INTO Movies(moviename,year,rating) VALUES(?,?,?)";

			try (Connection conn = this.connect();
					PreparedStatement pstmt = conn.prepareStatement(sql)) {
				pstmt.setString(1, name);
				pstmt.setString(2, year);
				pstmt.setString(3, rating);
				pstmt.executeUpdate();	        
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				return false;
			}			
		}
		test.log(LogStatus.INFO, "Inserted movie data into DB");
		return true;		

	}

	/**
	 * Printing movies info
	 * @return
	 */
	public boolean printMoviesInfo(){
		String sql = "SELECT moviename, year, rating FROM Movies";

		try (Connection conn = this.connect();
				Statement stmt  = conn.createStatement();
				ResultSet rs    = stmt.executeQuery(sql)){

			// loop through the result set
			while (rs.next()) {
				System.out.println(rs.getString("moviename") +  "\t" + 
						rs.getString("year") + "\t" +
						rs.getString("rating"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		test.log(LogStatus.INFO, "Printed movie data from DB");
		return true;
	}

	/**
	 * Establishing database connection
	 * @return
	 */
	public Connection connect() {
		Connection conn = null;
		try {
			// db parameters
			String url = "jdbc:sqlite:Database/test.db";
			// create a connection to the database
			conn = DriverManager.getConnection(url);            
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} 
		return conn;
	}	

}
