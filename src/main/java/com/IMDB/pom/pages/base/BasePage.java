package com.IMDB.pom.pages.base;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.IMDB.pom.pages.util.IMDBConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BasePage {

	public WebDriver driver;
	public ExtentTest test;

	/**
	 * Default constructor
	 */
	public BasePage(){

	}

	/**
	 * Parameterized constructor
	 * @param driver
	 * @param test
	 */
	public BasePage(WebDriver driver, ExtentTest test){
		this.driver = driver;
		this.test = test;		

	}

	/**
	 * Capturing screenshot
	 */
	public void takeScreenshot(){
		Date d = new Date();
		String screenshotFile=d.toString().replace(":","_").replace(" ", "_")+".png";	
		String filePath = IMDBConstants.REPORTS_PATH+"Screenshots//"+screenshotFile;
		//Store screenshots in that file
		File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try{
			FileUtils.copyFile(srcFile, new File(filePath));
		}catch(IOException e){
			e.printStackTrace();

		}

		test.log(LogStatus.INFO, test.addScreenCapture(filePath));
	}


	/**
	 * Reporting failed test case	
	 * @param failureMessage
	 */
	public void reportFailure(String failureMessage){
		test.log(LogStatus.FAIL, failureMessage);
		takeScreenshot();
		Assert.fail("Test failed");
	}


	/**
	 * Explicit wait	
	 * @param timeInMiliSeconds
	 */
	public void randomWait(int timeInMiliSeconds){

		try {
			Thread.sleep(timeInMiliSeconds);
		} catch (InterruptedException e) {}

	}	

	/**
	 * Checking if an element is present
	 * @param locator
	 * @return
	 */
	public boolean isElementPresent(String locator){
		test.log(LogStatus.INFO, "Trying to find the Required Element");
		int s = driver.findElements(By.xpath(locator)).size();

		if(s==0){
			test.log(LogStatus.INFO, "Required Element not found-> "+locator);
			return false;
		}
		else{
			test.log(LogStatus.INFO, "Found Required Element-> "+locator);
			return true;
		}
	}		

}

