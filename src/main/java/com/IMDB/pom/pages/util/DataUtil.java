package com.IMDB.pom.pages.util;

import java.util.Hashtable;

public class DataUtil {

	public static Object[][] getData(Xls_Reader xls, String testCaseName){
		String sheet = IMDBConstants.TESTDATA_SHEET;

		//System.out.println(sheet);
		//System.out.println(testCaseName);
		//System.out.println(xls);
		int testStartRowNum = 1;
		//System.out.println(xls.getCellData(sheet, 0, testStartRowNum));
		while(!xls.getCellData(sheet, 0, testStartRowNum).equals(testCaseName)){

			testStartRowNum++;
			//System.out.println(testStartRowNum);
		}
		//System.out.println("Test starts from row-"+testStartRowNum);
		int colStartRowNum = testStartRowNum+1;
		int dataStartRowNum = testStartRowNum+2;

		//Calculate rows of data
		int rows =1;
		while(!xls.getCellData(sheet, 0, dataStartRowNum+rows).equals("")){

			rows++;

		}
		//System.out.println("Total rows are - "+rows);
		int cols =0;
		while(!xls.getCellData(sheet, cols, colStartRowNum).equals("")){

			cols++;
		}
		//System.out.println("Total cols are -- "+cols);

		//Read the data
		Object[][] data = new Object[rows][1];
		int iData = 0;
		Hashtable<String,String> table = null;

		for(int rNum=dataStartRowNum;rNum<dataStartRowNum+rows;rNum++){
			table = new Hashtable<String,String>();
			for(int cNum=0;cNum<cols;cNum++){
				String key =xls.getCellData(sheet, cNum, colStartRowNum);
				String value =xls.getCellData(sheet, cNum, rNum);
				//data[iData][cNum]= (xls.getCellData(sheet, cNum, rNum));
				table.put(key, value);

			}
			data[iData][0]=table;
			iData++;
		}
		return data;

	}
	//True - for runmode Y
	//False -for runmode N
	public static boolean isSkip(Xls_Reader xls, String testName){

		int rows = xls.getRowCount("TestCases");
		for(int rNum=2;rNum<=rows;rNum++){
			String tcid = xls.getCellData("TestCases", "TCID", rNum);
			if(tcid.equals(testName)){
				String runmode= xls.getCellData("TestCases", "RunMode", rNum);
				if(runmode.equals("Y")){
					return true;
				}
				else return false;
			}

		}return false; // Default


	}

}

