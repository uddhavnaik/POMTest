package com.IMDB.pom.pages.util;

import java.util.Hashtable;

public class IMDBConstants {

	//Environment
	public static final String ENV = "STAGE"; //stage, QA, test 
	//MAINTENANCE PAGE
	public static final String MAINTENANCE_PAGE ="//h1[text()='IMDB IS UNDER MAINTENANCE.']";
	
	//Grid
	public static final boolean GRID_RUN = false;

	//URLs
	public static final String HOMEPAGE_URL = "http://www.imdb.com/";
	public static final String QA_HOMEPAGE_URL = "http://www.imdb.com/";


	public static final String CHROME_DRIVER_EXE = System.getProperty("user.dir")+"\\drivers\\chromedriver.exe";
	public static final String GECKO_DRIVER_EXE = System.getProperty("user.dir")+"\\drivers\\geckodriver.exe";
	public static final String IE_DRIVER_EXE = System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe";



	public static final String REPORTS_PATH = "D:\\reports\\IMDB\\";
	public static final String DATA_XLS_PATH = System.getProperty("user.dir")+"\\data\\Data.xlsx";
	public static final String TESTDATA_SHEET = "Data";
	public static final String TESTCASES_SHEET = "TestCases";

	//Data
	public static final Object RUNMODE_COL = "RunMode";


	//Home Page
	public static final String TOP_MOVIES_LINK = "//div[@class='table-cell primary']/a[contains (text(), 'Top Rated Indian Movies')]";
	public static final String TOP_MOVIES_PAGE_HEADER = "//h1[contains (text(), 'Top Rated Indian Movies')]";
	public static final String MOVIE_NAMES = "//td[@class='titleColumn']/a[contains (@href, '/title/')]";
	public static final String MOVIE_YEARS = "//span[@class='secondaryInfo']";
	public static final String MOVIE_RATINGS = "//strong[contains (@title, 'user ratings')]";	




	public static Hashtable<String, String> table;
	public static Hashtable<String, String> tableMBO;
	public static Hashtable<String, String> getEnvDetails(){
		if(table==null){
			table = new Hashtable<String, String>();
			if(ENV.equals("STAGE")){
				table.put("url", HOMEPAGE_URL);
			}
			else if(ENV.equals("QA")){
				table.put("url", QA_HOMEPAGE_URL);
			}

		}
		return table;

	}	

}
