package com.IMDB.pom.pages.util;


import java.io.File;
import java.util.Date;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {
	private static ExtentReports extent;

	public static ExtentReports getInstance() {
		if (extent == null) {
			Date d=new Date();
			String fileName="IMDB_"+d.toString().replace(":", "_").replace(" ", "_")+".html";
			String reportPath = IMDBConstants.REPORTS_PATH+fileName;
			extent = new ExtentReports(reportPath, true, DisplayOrder.NEWEST_FIRST);


			extent.loadConfig(new File(System.getProperty("user.dir")+"//ReportsConfig.xml"));
			// optional
			extent.addSystemInfo("Selenium Version", "3.4.0").addSystemInfo(
					"Environment", "STAGE");
		}
		return extent;
	}
}
