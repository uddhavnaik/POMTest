package com.IMDB.pom.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.IMDB.pom.pages.base.BasePage;
import com.IMDB.pom.pages.util.IMDBConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class IMDBHomePage extends BasePage {

	@FindBy(xpath=IMDBConstants.TOP_MOVIES_LINK)
	public WebElement topMoviesLink;

	@FindBy(xpath=IMDBConstants.TOP_MOVIES_PAGE_HEADER)
	public WebElement topMoviesPageHeader;

	/**
	 * Default constructor
	 */
	public IMDBHomePage() {}

	/**
	 * Parameterized constructor
	 * @param driver
	 * @param test
	 */
	public IMDBHomePage(WebDriver driver, ExtentTest test) {
		super(driver,test);
	}

	/**
	 * Navigating to Top movies page
	 * @return
	 */
	public boolean goToTopMovies() {

		randomWait(3000);
		test.log(LogStatus.INFO, "Navigating to Top movies page");
		topMoviesLink.click();

		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(topMoviesPageHeader));
			takeScreenshot();
			return  true;
		} catch (Exception e) {
			takeScreenshot();
			return false;
		}		

	}

}
